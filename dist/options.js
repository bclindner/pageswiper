/**
 * options.js: options page logic for Pageswiper
 * bclindner, 2023
 */

/**
 * Display a toast to the user.
 */
function toast(text, duration = "5s") {
  const el = document.getElementById("toast");
  el.innerText = text;
  el.style.animation = "";
  // use requestAnimationFrame to ensure styles are recomputed before re-adding animation
  // https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_animations/Tips#run_an_animation_again
  requestAnimationFrame((time) => {
    requestAnimationFrame((time) => {
      el.style.animation = "fadeout " + duration;
    });
  });
}

/**
 * Metadata for all tracked form fields.
 *
 * This is used by loadOptions and saveOptions to get and set the values from
 * the browser storage into the form.
 */
const data = [
  {
    name: "scrapers",
    get value() {
      const scrapers = [...document.forms[0]["scrapers"]];
      return scrapers
        .filter((el) => el.checked)
        .map((el) => el.value)
        .join(" ");
    },
    set value(value) {
      const values = value.split(" ");
      [...document.querySelectorAll('input[name="scrapers"]')].forEach(
        (input) => (input.checked = values.includes(input.value)),
      );
    },
  },
  {
    name: "webhookEnabled",
    get value() {
      return document.forms[0]["webhookEnabled"].checked;
    },
    set value(value) {
      document.forms[0]["webhookEnabled"].checked = value;
    },
  },
  {
    name: "webhookUrl",
    get value() {
      return document.forms[0]["webhookUrl"].value;
    },
    set value(value) {
      document.forms[0]["webhookUrl"].value = value;
    },
  },
  {
    name: "webhookType",
    get value() {
      return document.forms[0]["webhookType"].value;
    },
    set value(value) {
      document.forms[0]["webhookType"].value = value;
    },
  },
];

/**
 * Save the options into browser storage.
 *
 * This function runs on each form submission.
 */
function saveOptions(evt) {
  // create a browser-storage-compatible map of field name to (JSON stringified) field value
  const options = data.reduce(
    (a, b) => ({
      ...a,
      [b.name]: JSON.stringify(b.value),
    }),
    {},
  );
  // set the options, then display a toast
  browser.storage.sync.set(options).then(() => toast("saved"));
  evt.preventDefault();
}

/**
 * Load options from browser storage into the form.
 *
 * This function runs when the form is first loaded.
 */
function loadOptions(evt) {
  const fieldNames = data.map((field) => field.name);
  browser.storage.sync.get(fieldNames).then((results) => {
    data.forEach((field) => {
      if (field.name in results) {
        field.value = JSON.parse(results[field.name]);
      }
    });
  });
  evt.preventDefault();
}

// set up event handlers for saveOptions/loadOptions
document.addEventListener("DOMContentLoaded", loadOptions);
document.forms[0].addEventListener("submit", saveOptions);
