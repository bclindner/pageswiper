/**
 * view.js: page logic for browser metadata viewer in Pageswiper
 * bclindner, 2023
 */

const template = document.getElementById("template-result");
/**
 * Populate the popup.
 */
async function populate(tabId, tabInfo = null) {
  // get tab info
  if (tabInfo === null) {
    const { tabs } = await browser.storage.session.get("tabs");
    tabInfo = tabs[tabId]?.results || [];
  }
  // clear the current results (if any are there)
  const resultsEl = document.getElementById("results");
  resultsEl.innerHTML = "";
  // render a result for each item in the scraped metadata for this tab
  for (item of tabInfo) {
    const clone = template.content.cloneNode(true);
    clone
      .querySelector(".result-name")
      .appendChild(document.createTextNode(item.type));
    clone
      .querySelector(".result-value")
      .appendChild(document.createTextNode(JSON.stringify(item, null, 2)));
    resultsEl.appendChild(clone);
  }
}

browser.tabs.query({ currentWindow: true, active: true }).then((data) => {
  const [tab] = data;
  populate(tab.id);
  browser.runtime.onMessage.addListener((data, sender) => {
    if (sender.tab.id === tab.id) {
      populate(tab.id, data.results);
    }
  });
});
