/**
 * swiper.js: scraper for Pageswiper
 * bclindner, 2023
 */

/******** microformats ********/

const parseP = (name) => (el) => ({ [name]: [el.innerText] });
const parseU = (name) => (el) =>
  "href" in el.attributes ? { [name]: [el.attributes.href.value] } : {};

/***
 * Merge objects whose values are arrays by combining each object's array
 * values.
 */
const mergeObjectOfArrays = (mf1, mf2) => {
  for (let [key, value] of Object.entries(mf2)) {
    if (key in mf1) {
      mf1[key] = mf1[key].concat(value);
    } else {
      mf1[key] = value;
    }
  }
  return mf1;
};

const MICROFORMATS = {};

/**
 * Parse a microformat.
 */
const parseMicroformat = (rootClass, context) => (rootEl) => {
  // get a list of all fields
  const fields = Object.keys(MICROFORMATS[rootClass]);
  // select all supported metadata elements within the microformat
  const properties = [
    ...rootEl.querySelectorAll(fields.map((cls) => "." + cls).join(",")),
  ]
    // grab all its classes
    .map((el) =>
      el.className
        // split them up
        .split(" ")
        // filter out the ones we don't use
        .filter((cls) => fields.includes(cls))
        // run the parser on each
        .map((cls) => MICROFORMATS[rootClass][cls](el))
        // combine all declarations of each field
        .reduce(mergeObjectOfArrays, {}),
    )
    // combine all fields for the object
    .reduce(mergeObjectOfArrays, {});
  // add the other microformat metadata
  return {
    "@context": context,
    ...properties,
  };
};

MICROFORMATS["h-card"] = {
  "p-name": parseP("name"),
  "p-honorific-prefix": parseP("honorific-prefix"),
  "p-given-name": parseP("given-name"),
  "p-additional-name": parseP("additional-name"),
  "p-family-name": parseP("family-name"),
  "p-sort-string": parseP("sort-string"),
  "p-honorific-suffix": parseP("honorific-suffix"),
  "p-nickname": parseP("nickname"),
  "p-category": parseP("category"),
  "p-adr": parseP("adr"),
  "p-post-office-box": parseP("post-office-box"),
  "p-extended-address": parseP("extended-address"),
  "p-street-address": parseP("street-address"),
  "p-locality": parseP("locality"),
  "p-region": parseP("region"),
  "p-postal-code": parseP("postal-code"),
  "p-country-name": parseP("country-name"),
  "p-label": parseP("label"),
  "p-geo": parseP("geo"),
  "p-latitude": parseP("latitude"),
  "p-longitude": parseP("longitude"),
  "p-tel": parseP("tel"),
  "p-note": parseP("note"),
  "p-org": parseP("org"),
  "p-job-title": parseP("job-title"),
  "p-role": parseP("role"),
  "p-sex": parseP("sex"),
  "p-gender-identity": parseP("gender-identity"),
  "u-email": parseU("email"),
  "u-logo": parseU("logo"),
  "u-photo": parseU("photo"),
  "u-url": parseU("url"),
  "u-uid": parseU("uid"),
  "u-key": parseU("key"),
  "u-impp": parseU("impp"),
  // we don't have an easy way to parse the dates the microformat spec seems to
  // want, so as a result, so let's just grab the text itself
  "dt-anniversary": parseP("anniversary"),
  "dt-birthday": parseP("birthday"),
  "h-card": parseMicroformat("h-card"),
};

/******** end microformats ********/

// list of XFN relationships
// https://gmpg.org/xfn/1
const xfnNames = [
  "acquaintance",
  "friend",
  "met",
  "co-worker",
  "colleague",
  "co-resident",
  "neighbor",
  "child",
  "parent",
  "sibling",
  "spouse",
  "muse",
  "crush",
  "date",
  "sweetheart",
  "me",
];

/**
 * Function for getting the current URL.
 */
function getCurrentUrl() {
  const url = new URL(window.location.toString());
  url.hash = "";
  return url.toString();
}

let currentUrl = getCurrentUrl();

// All scraping functions
const scrapers = {
  // JSON+LD
  // https://json-ld.org/
  jsonld: () =>
    [...document.querySelectorAll('script[type="application/ld+json"]')].map(
      (el) => ({
        type: "json-ld",
        data: JSON.parse(el.innerText),
      }),
    ),
  // XFN
  // https://microformats.org/wiki/rel-me
  xfn: () => {
    const results = [...document.querySelectorAll(":is(a, area, link)[rel]")]
      .flatMap((el) =>
        el.attributes.rel.value
          .split(" ")
          .filter((rel) => xfnNames.includes(rel))
          .map((rel) => ({ [rel]: [el.attributes.href.value] })),
      )
      // map into JSON-LD(ish?) format
      .reduce(
        (obj, data) => ({
          ...obj,
          data: mergeObjectOfArrays(obj.data, data),
        }),
        {
          type: "xfn",
          data: { "@context": "https://gmpg.org/xfn/11" },
        },
      );
    if (Object.keys(results.data).length <= 1) {
      return [];
    } else {
      return results;
    }
  },
  // microformats
  // https://microformats.org/wiki/microformats2-parsing
  hcard: () =>
    [...document.querySelectorAll(".h-card")].map((el) => ({
      type: "hCard",
      data: parseMicroformat("h-card", "http://www.w3.org/2006/vcard/ns")(el),
    })),
  // opengraph (rough)
  // https://ogp.me/
  opengraph: () => {
    const results = [...document.querySelectorAll('meta[property^="og:"]')]
      // ensure property and content are both set
      .filter(
        (el) =>
          el.attributes.property.value !== "" &&
          el.attributes.content.value !== "",
      )
      // map key to value
      .map((el) => ({
        [el.attributes.property.value.substring(3)]: [
          el.attributes.content.value,
        ],
      }))
      // build object with reduce()
      .reduce(
        (obj, data) => ({
          ...obj,
          data: mergeObjectOfArrays(obj.data, data),
        }),
        { type: "opengraph", data: { "@context": "http://ogp.me/ns" } },
      );
    if (Object.entries(results.data).length <= 1) {
      return [];
    } else {
      return [results];
    }
  },
};

let enabledScrapers = Object.keys(scrapers);

/**
 * Scrape the page for all data the user requested, and send it, if it exists
 */
const scrape = (url) => {
  const results = enabledScrapers
    // get all enabled scraper functions
    .map((scraperName) => scrapers[scraperName])
    // run each
    .map((scraper) => scraper())
    // flatten results
    .flat();
  // don't send if there is no data
  if (results.length > 0) {
    // this passes the data to the sender
    browser.runtime.sendMessage({
      url,
      date: new Date().toISOString(),
      results,
    });
  }
};

function startScraper() {
  // set up an observer
  new MutationObserver(() => {
    // check if the URL has changed (in any meaningful way, at least)
    const newUrl = getCurrentUrl();
    if (currentUrl !== newUrl) {
      // Set the new URL and run the scraper
      currentUrl = newUrl;
      scrape(currentUrl);
    }
  }).observe(document, {
    subtree: true,
    attributes: true,
    childList: true,
  });
  // run a scrape as soon as the mutation observer is up, regardless of its state
  scrape(currentUrl);
}

// grab the scraper config, then start scraping
browser.storage.sync.get("scrapers").then((data) => {
  if ("scrapers" in data) {
    enabledScrapers = JSON.parse(data["scrapers"]).split(" ");
  }
  startScraper();
});
