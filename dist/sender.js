/**
 * sender.js: background processing script for Pageswiper
 * bclindner, 2023
 */

async function updateTabInfo(tabId, data) {
  const { tabs } = await browser.storage.session.get("tabs");
  const tabInfo = {
    ...tabs,
    [tabId]: data,
  };
  // set data in session
  await browser.storage.session.set({ tabs: tabInfo });
  if (data.results.length > 0) {
    // set badge text
    browser.action.setBadgeText({
      tabId,
      text: data.results.length.toString(),
    });
  }
}

const handlers = {
  json: {
    serialize: JSON.stringify,
    options: {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
    },
  },
  discord: {
    // discord serializer just prettifies the JSON, wraps it in a markdown code
    // block, then wraps that in a "content" field to comply with discord webhook
    // format
    serialize: (data) =>
      JSON.stringify({
        content: "```json\n" + JSON.stringify(data, null, 2) + "\n```",
      }),
    options: {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
    },
  },
};

function sendData(data, sender) {
  // if this was sent by a tab (it should be), add it to the local storage
  if (sender.tab?.id !== undefined) {
    updateTabInfo(sender.tab.id, data);
  }
  // send via webhook, if necessary
  browser.storage.sync
    .get(["webhookEnabled", "webhookUrl", "webhookType"])
    .then((result) => {
      const webhookEnabled = JSON.parse(result.webhookEnabled);
      const webhookUrl = JSON.parse(result.webhookUrl);
      const webhookType = JSON.parse(result.webhookType);
      const handler = handlers[webhookType || "json"];
      if (webhookEnabled && data.results.length > 0) {
        fetch(webhookUrl, {
          ...handler.options,
          body: handler.serialize(data),
        });
      }
    });
}

browser.runtime.onMessage.addListener(sendData);
