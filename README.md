# Pageswiper

Pageswiper is an extension that passively scrapes metadata from the sites you
visit. You can then view the data (in JSON format) from the extension's icon.

Optionally, it can send all the data it scrapes from each page to an HTTP
webhook of your choosing, along with the URL and retrieval date. This can be
good for collecting data if you're doing research, or if you're just curious
about the metadata you might be missing while you browse.

Currently, this has only been tested on Firefox, but as this was built to
standard, it should ideally work on Chromium-based browsers too. (Due to some
API inconsistencies, there is a good chance this extension won't work on Safari,
or iOS.)

⚠️ **Disclaimer:** This is an experimental extension made solely for research
purposes. It is subject to change, and may carry some security risks, such as:

- Scraping sensitive info from a private page (e.g. a bank website) and sending
  it to your webhook
- Maliciously crafted pages that may put your browser or webhook at risk
- Personal data collection/tracking concerns, particularly if using a webhook
  whose data is publicly accessible

Generally speaking, I recommend making sure whatever you're using for a webhook
is either reasonably secure or expendable, and be prepared for things to break
occasionally as this extension's usefulness is pinned down.

## Supported Formats

Currently, this extension scrapes the following types of metadata:

- [JSON-LD] `<script>` tags
- [XHTML Friends Network] hyperlinks
  - This includes `rel="me"`
- [hCard] data (experimental)
- [Open Graph] data

Eventually I have interest in supporting the following:

- More robust parsing for h-card data and other [microformats2] formats like
  h-entry, h-feed, etc.
- Other common `<meta>` tags, like [Facebook App Links], [Twitter Card] tags,
  etc.
- Exports for JSON-LD, Turtle, and [SPARQL Graph Store] protocol

There are no plans to support any platform-specific scraping - ideally custom
scrapers, user scripts, or other WebExtensions can handle that for you.

## Why?

These days, for accessibility, convenience, and just plain old SEO, nearly every
publisher and Web page embeds some metadata to make it more discoverable. Here
are some examples I've found:

- Nearly every major Web site provide metadata tags for Open Graph, which
  provides most info you would see in an app embed
- Many social sites implement hCards on their user profile pages
- Some sites offer a full representation of major site elements as JSON-LD:
  - Search results
  - Breadcrumbs
  - Recipes
  - Product data
  - Search engine info
- hCard representations of certain user profiles
- etc.

This is mostly just a web toy, but it can also offer you an easy way to get
machine-readable metadata out of the Web sites you visit, for your own data
processing.

[JSON-LD]: https://json-ld.org/
[XHTML Friends Network]: https://gmpg.org/xfn/
[hCard]: https://microformats.org/wiki/h-card
[Open Graph]: https://ogp.me/
[Facebook App Links]: https://developers.facebook.com/docs/applinks/metadata-reference/
[microformats2]: https://microformats.org/wiki/Main_Page
[Twitter Card]: https://developer.twitter.com/en/docs/twitter-for-websites/cards/overview/markup
[Web App Manifests]: https://developer.mozilla.org/en-US/docs/Web/Manifest
[Semantic Web]: https://en.wikipedia.org/wiki/Semantic_Web
[SPARQL Graph Store]: https://www.w3.org/TR/2013/REC-sparql11-http-rdf-update-20130321/
